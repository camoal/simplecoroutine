package com.camoal.simplecoroutine

import kotlinx.coroutines.CoroutineScope

class Coroutine {

    companion object {

        /**
         * Crea el observador.
         *
         * @param block suspend CoroutineScope.() -> T
         * @return CoroutineObserver<T>
         */
        fun <T> create(block: suspend CoroutineScope.() -> T): CoroutineObserver<T> {
            return CoroutineObserver(block)
        }
    }
}