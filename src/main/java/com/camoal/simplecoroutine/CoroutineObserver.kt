package com.camoal.simplecoroutine

import kotlinx.coroutines.*

@Suppress("EXPERIMENTAL_API_USAGE")
class CoroutineObserver<T>(private val block: suspend CoroutineScope.() -> T) {

    private var observerDispatcher: CoroutineDispatcher = Dispatchers.IO
    private var subscriberDispatcher: CoroutineDispatcher = Dispatchers.Main

    private lateinit var before: () -> Unit
    private lateinit var after: () -> Unit

    /**
     * Se subscribe con resultado satisfactorio.
     *
     * @param success (T) -> Unit
     * @return Job
     */
    fun subscribe(success: (T) -> Unit): Job {
        return subscribe(success, {})
    }

    /**
     * Se subscribe con resultado satisfactorio y de error.
     *
     * @param success (T) -> Unit
     * @param error (Throwable) -> Unit
     * @return Job
     */
    fun subscribe(success: (T) -> Unit, error: (Throwable) -> Unit): Job {
        return launch {
            CoroutineScope(observerDispatcher).launch{
                try {
                    if(::before.isInitialized) {
                        invokeSubscriber(before)
                    }
                    val result = block()
                    launch(subscriberDispatcher){
                        success(result)
                        if(::after.isInitialized) {
                            after()
                        }
                    }
                }
                catch (t: Throwable){
                    if(t !is CancellationException) {
                        error.invoke(t)
                    }
                }
            }
        }
    }

    /**
     * Inicia en modo normal o test en función del dispatcher.
     *
     * @param block () -> Job
     * @return Job
     */
    private fun launch(block: () -> Job): Job {
        return when(observerDispatcher == Dispatchers.Unconfined){
            true -> runBlocking {
                block()
            }
            false -> block()
        }
    }

    /**
     * Invoca un valor en el subscriptor.
     *
     * @param block () -> Unit
     */
    private fun invokeSubscriber(block: () -> Unit) {
        CoroutineScope(subscriberDispatcher).launch {
            block()
        }
    }

    /**
     * Se modifican los dispatchers para el modo test.
     *
     * @return CoroutineTestObserver<T>
     */
    fun test(): CoroutineTestObserver<T> {
        observerDispatcher = Dispatchers.Unconfined
        subscriberDispatcher = Dispatchers.Unconfined
        return CoroutineTestObserver(this)
    }

    /**
     * Operación que se realiza al comienzo.
     *
     * @param block () -> Unit
     * @return CoroutineObserver<T>
     */
    fun doOnSubscribe(block: () -> Unit): CoroutineObserver<T> = apply {
        this.before = block
    }

    /**
     * Operación que se realiza al finalizar.
     *
     * @param block () -> Unit
     * @return CoroutineObserver<T>
     */
    fun doAfterTerminate(block: () -> Unit): CoroutineObserver<T> = apply {
        this.after = block
    }

    /**
     * Dispatcher en el que observa el subscriptor.
     *
     * @param dispatcher CoroutineDispatcher
     * @return CoroutineObserver<T>
     */
    fun observeOn(dispatcher: CoroutineDispatcher): CoroutineObserver<T> = apply {
        subscriberDispatcher = dispatcher
    }
}
