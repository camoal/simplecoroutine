package com.camoal.simplecoroutine

import kotlinx.coroutines.Job

/**
 * Cancela una lista de job
 *
 * @receiver MutableList<Job>
 */
fun MutableList<Job>.cancelAll(){
    forEach { it.cancel() }
}