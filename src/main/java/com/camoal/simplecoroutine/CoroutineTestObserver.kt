package com.camoal.simplecoroutine

class CoroutineTestObserver<T>(private val observer: CoroutineObserver<T>){

    /**
     * Comprueba el valor del test.
     *
     * @return T?
     */
    fun assertValue(value: T): T? {
        var result: T? = null
        observer.subscribe(
                {
                    result = it
                },
                {
                    it.printStackTrace()
                })

        if(result == null || result != value){
            throw AssertionError()
        }
        return result
    }
}