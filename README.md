# Simple Coroutine

Simple Coroutine es una librería que permite realizar coroutines de forma simple y segura.

  - Las coroutines se ejecutan de forma segura evitando excepciones no controladas.
  - Realiza operaciones de forma asíncrona evitando bloqueos.
  - Ejecuta test de instrumentación y unitarios de forma simple.
  - Realiza acciones antes y después de finalizar la coroutine.

# Requisitos

Añadir las siguientes dependencias en el build.gradle de la aplicación:

```sh
    Kotlin: 1.3+
```

```sh
    implementation "org.jetbrains.kotlinx:kotlinx-coroutines-core:1.1.1"
```

```sh
    implementation 'org.jetbrains.kotlinx:kotlinx-coroutines-android:1.1.1'
```

```sh
    implementation "com.camoal.simplecoroutine:simplecoroutine:1.0.0"
```

Añadir la siguiente url de maven en el build.gradle del proyecto dentro de "repositories":

```sh
    maven { 
        url 'https://camoal89.bintray.com/maven' 
    }
```

Una vez añadido el build.gradle del proyecto debe mostrarse de la siguiente forma:

```sh
    repositories {
            google()
            jcenter()
            maven {
                url 'https://camoal89.bintray.com/maven'
            }
        }
```

# Crear coroutine

 Crear una coroutine es muy simple. El siguiente ejemplo muestra como crear una coroutine en la que se esperará 5 segundos antes de devolver el resultado "Hello simple coroutine". La coroutine de ejemplo se encuentra dentro de la función "execute" y devuelve un objeto de tipo "CoroutineObserver<String>".

```sh
     class GetGreetingUseCase{
     
        fun execute(): CoroutineObserver<String> {
            return Coroutine.create {
                Thread.sleep(5000)
                "Hello simple coroutine"
            }
        }
    }
```

Para obtener el resultado de la coroutine de forma más simple es necesario llamar al método que devuelve un "CoroutineObserver" y tras esto realizar la siguiente procedimiento:

```sh
    GetGreetingUseCase()
            .execute()
            .subscribe { Se obtiene el resultado esperado "Hello simple coroutine" }
```

# Acciones avanzadas

La librería soporta más acciones que proporcionan un mayor control sobre el flujo de ejecución:

  - doOnSubscribe: Realiza una acción antes de ejecutar la coroutine.
  - doAfterTerminate: Realiza una acción después de ejecutar la coroutine.
  - observeOn: Elige si se observa el resultado en el hilo principal (Dispatchers.Main) o secundario (Dispatchers.IO). En caso de no definir este método se observa en el hilo principal.
  - subscribe: Realiza la subscripción para obtener el resultado.
        - La primera lambda de "subscribe" hace referencia al resultado esperado.
        - La segunda lambda de "subscribe" hace referencia a una excepción que ha ocurrido.

```sh
    GetGreetingUseCase()
            .execute()
            .doOnSubscribe { Acción a realizar antes }
            .doAfterTerminate { Acción a realizar después }
            .observeOn(Dispatchers.IO)
            .subscribe(
                    {
                        Se obtiene el resultado esperado "Hello simple coroutine"
                    },
                    {
                        Excepción controlada
                    })
```
# Cancelar coroutine

Es posible cancelar una coroutine. Para ello es necesario crear una lista de tipo "Job" y añadir la coroutine como se muestra a continuación.

```sh

    val jobs = mutableListOf<Job>()

    val job = GetGreetingUseCase()
            .execute()
            .subscribe { Se obtiene el resultado esperado "Hello simple coroutine" }
            
    jobs.add(job)
    
```

Para cancelar todas las coroutines de una lista es necesario llamar al siguiente método:

```sh

    jobs.cancelAll()
    
```

# Test


Para realizar un test es necesario usar ".test" y a continuación ".assertValue" con el valor que se quiere validar.


```sh
    @Test
    fun testGreeting() {
         GetGreetingUseCase()
                .execute()
                .test()
                .assertValue("Hello simple coroutine")
    }
```